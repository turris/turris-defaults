from setuptools import setup

DESCRIPTION = """
Package of default setings.
"""

setup(
    name="turris-defaults",
    version='0.0.1',
    author='CZ.NIC, z.s.p.o. (http://www.nic.cz/)',
    author_email='packaging@turris.cz',
    packages=['turris_defaults']
    url='https://gitlab.nic.cz/turris/foris-controller/foris-controller',
    license='COPYING',
    description=DESCRIPTION,
    long_description=open('README.md').read(),

)
