# Turris Defaults

Project to hold initial default settings that are distributed across Turris projects.

In case any setting is updated removed, changes should travel downstream.
